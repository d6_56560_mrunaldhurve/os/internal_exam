#!/bin/bash

# script to find first n prime numbers

clear

echo "Enter the number : "
read num

echo "First $num prime numbers are : "

i=2
count=1

while [ $count -le $num ]
do
        flag=0
        for (( j=2 ; j <= ($i / 2) ; j++ ))
        do
                if [ `expr $i % $j` -eq 0 ]
                then
                        flag=1;
                        break;
                fi
        done
        if [ $flag -eq 0 ]
        then
                echo $i
                count=`expr $count + 1`
        fi
        i=`expr $i + 1`
done

exit